const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute");

const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect('mongodb+srv://admin:admin123@cluster0.xovnn.mongodb.net/toDo176?retryWrites=true&w=majority',{

		useNewUrlParser:true,
		useUnifiedTopology:true

	});

let db = mongoose.connection;
db.on('error', console.error.bind(console,"Connection Error"));
db.once('open',() => console.log("connected to MongoDB"));

app.use("/tasks", taskRoute);



app.listen(port,() => console.log(`Server running at port ${port}`));