//Contains all the endpoints for our application
//We separate the routes such that "app.js" only contains information on the server

const express = require("express")

//Create a Router instance that fucntions as a middleware and routing system.
//Allow access to HTTP method middlewares that makes it easier to create routes for our application.
const router = express.Router();

const taskController = require("../controllers/taskControllers");

//[Routes]
//Route for retrieving all the tasks
	router.get("/",(req,res) => {
		taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
	})

//Route for creating a task
	router.post("/",(req,res) => {
		taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
	})

//Route for deleting a task
	//callout the routing component to register a brand new endpoint.
	//when integratinga path variable within the URI, you are also changing the behaviour of the path from STATIC to DYNAMIC.
	//2 types of endpoint
		//Static route
			// -> unchanging, fixed, constant, steady
		//Dynamic route
			// -> interchangable or not fixed
	router.delete('/:task',(req,res)=>{
		//identify the task to be executed within this endpoint.
		//callout the proper function for this route and identify the source of the function.
		console.log(req.params.task)
		//place the value of the path variable inside its own container.
		let taskId = req.params.task
		//res.send('correct'); //= checker

		//retrieve the ID of the task by inserting the object ID of the resource.
		//make sure to pass down the identity of the task using the proper reference (objectID), however this time we are going to include the information as a path varuiable
		//path variable -> will allow us to inser data within the scope of the URL
		//path variable is useful when inserting a single piece of information
		// this is also useful when passing down information to REST API methods that does NOT include a BODY section like 'GET'.
		taskController.deleteTask(taskId).then(resultFromDelete => res.send(resultFromDelete))
	})

//Route for updating task status (Pending -> Complete)
	// lets create a dynamic endpoint for this brand new route.
	router.put('/:task',(req,res) => {
		//check you are able to acquire the values inside the path variables.
		console.log(req.params.task);
		let taskId = req.params.task;

		//call the intended controller to execute the process make sure to identify the provider of the function
		//after invoking the correct controller method handle the outcome.
		taskController.taskCompleted(taskId).then(outcome => {res.send(outcome)});
	}); 
//Route for updating task status (Complete -> Pending)
	router.put('/:task/pending',(req,res) => {
		let id = req.params.task
		//console.log(id);
		//declare the business logic aspect of this brand new task in our application.
		taskController.taskPending(id).then(outcome => {res.send(outcome)});
	});


//Use "module.exports" to export the router object to be used in the "app.js"
module.exports = router;