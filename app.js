//[SECTION] Dependencies and Modules
	const express = require("express");
	const mongoose = require("mongoose");

//[SECTION] Server Setup
	//establish a connection
	const app = express();
	//define a path/address 
	const port = 4000;
//[SECTION] Database Connection
	//Connect to MongoDB Atlas
	mongoose.connect('mongodb+srv://admin:admin123@cluster0.xovnn.mongodb.net/toDo176?retryWrites=true&w=majority',{

		//options to add to avoit deprecation warnings because of mongoose/mongoDB update.
		useNewUrlParser:true,
		useUnifiedTopology:true

	});

	//create notifications if the connection to the database is a success or a failure.
	let db = mongoose.connection;
	//let's add an on() from our mongoose connection to show if the connection has succeeded or failed in both the terminal and in the browser for our client.

	db.on('error', console.error.bind(console,"Connection Error"));
	//once the connection is open and successful, we will output a message in the terminal.
	db.once('open',() => console.log("connected to MongoDB"));

	//Middleware - a middleware, in expressjs context, are methods, functions that acts and adds features to our application.
	app.use(express.json());

	//Schema
	//Before we can create documents from our api to save into our databases, we first have to determine the structure of the document to be written in our database. This is to ensure the consistency of our document and avoid future errors.

	//Schema acts as a blueprint for our data/document.

	//Scheme() constructor from mongoose to create a new scheme object
	const taskSchema = new mongoose.Schema({

		/*
			Define the fields for our doducments.
			we will also be able to determine the appropriate date type of the value.
		*/

		name: String,
		status: String
	})

	//Mongoose Model
	/*
		Models are use to connect your API to the corresponding collection in your database. it is a representation of your collection.

		Models uses schemas to create object that correspond to the schema. By default, when creating a collection from your model, the collection name is pluralized,

		mongoose.model(<nameOfCollectionInAtlas>),<schemaToFollow>

	*/

	const Task = mongoose.model("task",taskSchema);

	//Post route to crea a new task
	app.post('/tasks',(req,res) =>{

		//when creating a new post/put or any route that requires data from the client, first console log your req.body or any part of the request that contains the data.

		//console.log(req.body);

		//creating a new task documents by using the constructor of our task model. this constructor will follow the scheme of the model.
		let newTask = new Task({

			name: req.body.name,
			status: req.body.status

		})

		//.save() from an obejct created by a model.
		//save() will allow us to save our document by connecting to our collection via our model.

		//save() has 2 approaches: 
		//1. we can add an anonymous function to handle created document or error.
		//2. we can add .then() chain which will allow us to handle errors and created documents in separate functions.
	/*	newTask.save((error,savedTask)=>{

			if(error){
				res.send("Document creation failed.");
			} else {
				res.send(savedTask)
			}

		})*/

		//.then() and .catch() chain 
		//.then() is used to handle the proper result/returned value of a function. If the function returns a value, we can run a separate function to handle it.
		//.catch()  is used to catch the error from the use of a function. So that if an error occurs, we can handle the error separately.

		newTask.save()
		.then(result => res.send({message: "Document Creation Successful"}))
		.catch(error => res.send({message: "Error in Document Creation"}));

	})

	const sampleSchema= new mongoose.Schema({

		name: String,
		isActive: Boolean
	})

	const Sample = mongoose.model("samples",sampleSchema);

	app.post('/samples',(req,res) =>{
		let newSample = new Sample({
			name: req.body.name,
			isActive: req.body.isActive
		})

		newSample.save((error,savedSample) =>{
			if (error) {
				res.send(error);
			} else {
				res.send(savedSample);
			}
		})
	})

	app.get('/samples',(req,res)=>{
		Sample.find({},)
		.then(result => res.send(result))
		.catch(error => res.send(error));
	})

	/*
		Create a new GET request method route to get all sample documents 
		send the array of sample documents in our postman client
		else, catch the error and send the error in the client
	*/

	//GET Method Request to retrieve all task documents from our collection

	app.get('/tasks',(req,res) => {
		//to query using mongoose, first access the model of the collection you want to manipulate
		//Model.find() in mongoose is similar in function to mongoDB's db.collection.find()
		//mongodb - db.tasks.find({})

		Task.find({},)
		.then(result => res.send(result))
		.catch(err => res.send(error));
	})

	//Schema

	/*const manggagamitSchema = new mongoose.Schema({
        username: String,
        isAdmin: Boolean
    })

    const Manggagamit = mongoose.model("manggagamit",manggagamitSchema);

    app.post('/manggagamit', (req,res)=>{
        let newManggagamit = new Manggagamit ({
            username:req.body.username,
            isAdmin:req.body.isAdmin
        })

        newManggagamit.save((savedManggagamit,error)=>{
            if(error){
                res.send(error);
            } else {
                res.send(savedManggagamit);
            }
        })
    })
*/
//[SECTION]Entry Point Response
	//bind the connection to the designated port
	app.listen(port,() => console.log(`Server running at port ${port}`));



//[ACTIVITY]

//[POST]
	const userSchema = new mongoose.Schema({
		username: String,
		password: String
	})

		const User = mongoose.model("user",userSchema);
		app.post('/users',(req,res) =>{
		let newUser = new User({

			username: req.body.username,
			password: req.body.password

		})

	newUser.save()
		.then(result => res.send({message: "Document Creation Successful"}))
		.catch(error => res.send({message: "Error in Document Creation"}));

	})

//[GET]
	app.get('/users',(req,res) => {
		User.find({},)
			.then(result => res.send(result))
			.catch(err => res.send(error));
	})